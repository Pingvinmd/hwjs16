function generalizedFibonacci(F0, F1, n) {
  if (n === 0) {
    return F0;
  }
  if (n === 1) {
    return F1;
  }
  var previous1 = F0;
  var previous2 = F1;
  var current = 0;
  for (var i = 2; i <= Math.abs(n); i++) {
    current = previous1 + previous2;
    previous1 = previous2;
    previous2 = current;
  }
  if (n < 0 && n % 2 === 0) {
    return -current;
  }
  return current;
}

var F0 = parseInt(prompt("Введите значение F0:"));
var F1 = parseInt(prompt("Введите значение F1:"));
var n = parseInt(prompt("Введите номер обобщенного числа Фибоначчи:"));

var result = generalizedFibonacci(F0, F1, n);

alert("Обобщенное число Фибоначчи с номером " + n + " равно " + result);